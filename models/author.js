var moment = require("moment");
var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var AuthorSchema = new Schema({
  first_name: { type: String, required: true, max: 100 },
  family_name: { type: String, required: true, max: 100 },
  date_of_birth: { type: Date },
  date_of_death: { type: Date }
});

// Virtual for author's full name
AuthorSchema.virtual("name").get(function() {
  // To avoid errors in cases where an author does not have either a family name or first name
  // We want to make sure we handle the exception by returning an empty string for that case

  var full_name = "";
  if (this.first_name && this.family_name) {
    full_name = this.family_name + ", " + this.first_name;
  }
  if (!this.first_name || !this.family_name) {
    full_name = "";
  }

  return full_name;
});

// Virtual for author's lifespan
AuthorSchema.virtual("lifespan").get(function() {
  return this.date_of_birth_formatted + " - " + this.date_of_death_formatted;
});

AuthorSchema.virtual("date_of_birth_formatted").get(function() {
  return this.date_of_birth
    ? moment(this.date_of_birth)
        .utc()
        .format("MMMM Do, YYYY")
    : "";
});

AuthorSchema.virtual("date_of_birth_formatted_form").get(function() {
  return this.date_of_birth
    ? moment(this.date_of_birth)
        .utc()
        .format("YYYY-MM-DD")
    : "";
});

AuthorSchema.virtual("date_of_death_formatted").get(function() {
  return this.date_of_death
    ? moment(this.date_of_death)
        .utc()
        .format("MMMM Do, YYYY")
    : "";
});

AuthorSchema.virtual("date_of_death_formatted_form").get(function() {
  return this.date_of_death
    ? moment(this.date_of_death)
        .utc()
        .format("YYYY-MM-DD")
    : "";
});

// Virtual for author's URL
AuthorSchema.virtual("url").get(function() {
  return "/catalog/author/" + this._id;
});

//Export model
module.exports = mongoose.model("Author", AuthorSchema);
