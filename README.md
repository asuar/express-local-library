## express-local-library

Created with Node,Express and Pug as part of [The Odin Project Curriculum](https://www.theodinproject.com/courses/nodejs/lessons/express-105-forms-and-deployment).
This project was a tool for me to practice with Node concepts and the Express framework.
